# Empiri gL-page
> Empirical Investigation Resources

Empirical evidence is the information received by means of the senses, particularly by observation and documentation of patterns and behavior through experimentation. The term comes from the Greek word for experience, ἐμπειρία (empeiría).

After Immanuel Kant, in philosophy, it is common to call the knowledge gained a posteriori knowledge (in contrast to a priori knowledge). 
